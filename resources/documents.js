const documents = [];

class document {

    constructor(params){
        this.id = params.id,
        this.title = params.title,
        this.details = params.details
    }

    static list(){
        return this.documents;
    }

    static create(params) {
        const document = new this(params)
        documents.push(document)
        return document
    }

    static find(id) {
        const document = documents.find((i) => i.id === Number(id));
        if(!document){ return null }

        return document;
    }

    static getDetaiil(id){
        const document = documents.find((i) => i.id === Number(id));
        if(!document){ return null }

        return document.details.join('\n')
    }
}

export default document