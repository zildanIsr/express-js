import Express from "express"
import { getDetail, updateData } from "../controller/profileController.js"


const router = Express.Router();

router.get('/detail', getDetail);
router.put('/edit', updateData);

export default router