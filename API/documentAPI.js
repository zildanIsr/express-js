import Express from "express"
import { create, find, list, getDetail, destroy } from "../controller/documentController.js"


const router = Express.Router();

router.get('/list', list);
router.post('/create', create);
router.get('/find/:id', find);
router.get('/detail/:id', getDetail);
router.delete('/delete/:id', destroy);

export default router