import express from 'express'
import { paramBio, check, mainRoute, viewengine, viewpara } from './controller/basicController.js'
import { testAuth } from './middleware/basicMiddleware.js'
// import { get as getProduct ,create as createProduct,
//     update as updateProduct ,destroy as deleteProduct,
//     find as findProduct } from "./controller/productController.js";
import productApi from './API/productAPI.js';
import bookApi from './API/bookAPI.js';
import documentApi from './API/documentAPI.js';
import profiletApi from './API/profileAPI.js';


const router =  express.Router()

router.get('/', mainRoute)
router.get('/check-health', check)
router.get('/bio', testAuth, paramBio)
router.get('/view', viewengine)
router.get('/viewparaf', viewpara)
router.use('/product', productApi)
router.use('/book', bookApi)
router.use('/document', documentApi)
router.use('/profile', profiletApi)

// router.get('/product/list', getProduct)
// router.post('/product/create', createProduct)
// router.put('/product/update', updateProduct)
// router.delete('/product/delete', deleteProduct)
// router.post('/product/find', findProduct)


export  { router }