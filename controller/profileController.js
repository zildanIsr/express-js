import Profile from '../resources/profile.js'

export const getDetail = (req, res) => {
    const profile = Profile.getDetaiil(req.params.id)

    return res.status(200).json(profile)
}

export const updateData = (req, res) => {
    const profile = Profile.editProfile(req.params.id);

    profile.update(req.body)

    res.status(200).json(profile);

}
  