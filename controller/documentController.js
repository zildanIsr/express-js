import Document from '../resources/documents.js'

export const create = (req, res) => {
    const document = Document.create(req.body)

    return res.status(200).json(document)
}

export const list = (req, res) => {
    const documents = Document.list();

    return res.status(200).json(documents)
}

export const find = (req, res) => {
    const document = Document.find(req.params.id);

    return res.status(200).json(document)
}

export const getDetail = (req, res) => {
    const document = Document.getDetaiil(req.params.id)

    return res.status(200).json(document)
}

export const destroy = (req, res) => {
    const document = Document.find(req.params.id);
    if (!document) return res.status(404).json({
      error: "Document not found!"
    });
  
    document.delete();
  
    res.status(204).end();
}

export const edit = (req, res) => {
    const document = Document.find(req.params.id);
    if (!document) return res.status(404).json({
      error: "Document not found!"
    });
  
    document.update(req.body);
  
    res.status(200).json(document);
}
  